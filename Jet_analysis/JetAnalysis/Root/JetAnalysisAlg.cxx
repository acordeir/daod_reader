#include "JetAnalysis/JetAnalysisAlg.h"
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include "StoreGate/ReadHandle.h"


JetAnalysisAlg :: JetAnalysisAlg (const std::string& name,ISvcLocator *pSvcLocator):
    AthAlgorithm (name, pSvcLocator)
    , m_ntsvc("THistSvc/THistSvc", name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  //declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
 //                  "Minimum electron pT (in MeV)" );
 // declareProperty( "SampleName", m_sampleName = "Unknown",
 //                  "Descriptive name for the processed sample" );

}



StatusCode JetAnalysisAlg :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ATH_MSG_INFO ("in initialize");

    
  // Book the variables to save in the *.root 
  m_Tree = new TTree("dumpedData", "dumpedData");
  ATH_MSG_DEBUG("Booking branches...");
  bookBranches(m_Tree); // book TTree with branches
  if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [dumpedData]");
      return StatusCode::FAILURE;
    }
  return StatusCode::SUCCESS;
}


StatusCode JetAnalysisAlg :: execute ()
{
  //retrieve the eventInfo object from the event store 
  const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  
  // print out run and event number from retrieved object
  ATH_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  // truth matching 
  if(!dumpTruthLLP()) ATH_MSG_DEBUG("ALP Truth Event information cannot be collected!");
  
  ATH_MSG_INFO ("After dumpTruthLLP");

  if(!dumpTruthZll()) ATH_MSG_DEBUG("ALP Truth Event information cannot be collected!");
  ATH_MSG_INFO ("After dumpTruthZll");


  // loop over jets in the container
  const xAOD::JetContainer* jets = nullptr;
  //ATH_CHECK (evtStore()->retrieve(jets, "AntiKt4EMPFlowJets"));
  ATH_CHECK (evtStore()->retrieve(jets, "AntiKt4EMTopoJets"));
  ATH_MSG_INFO("Number of jets: " <<  jets->size());

  for (const xAOD::Jet* jet : *jets){ //loop over jets in container
    ATH_MSG_INFO ("Jet:   pt: " << jet->pt() << " MeV" << ". Eta; Phi: " << jet->eta() << "; " << jet->phi());

    double jetE    = jet->e();
    double jetPt   = jet->pt();
    double jetEta  = jet->eta();
    double jetPhi  = jet->phi();
    float dR2;

    for (const xAOD::TruthParticle* part : *mc_alp){
    dR2  = std::pow( std::acos( std::cos( jetPhi - part->phi() ) ) , 2 );
    dR2 += std::pow( jetEta - part->eta() , 2 );
    
    if (dR2<= 0.2 * 0.2){
      ATH_MSG_INFO(part->pdgId()<<" match with jet (eta,phi) :" << jet->eta() << ", " << jet->phi());
      mc_alp_jet_e->push_back(jetE);
      mc_alp_jet_pt->push_back(jetPt);
      mc_alp_jet_eta->push_back(jetEta);
      mc_alp_jet_phi->push_back(jetPhi);
      break;
      }
    }
    ATH_MSG_INFO("befor push back: ");

    j_jetE->push_back(jetE);
    j_jetEta->push_back(jetEta);
    j_jetPhi->push_back(jetPhi);
    j_jetPt->push_back(jetPt);
  } 
  // end of loop

  m_Tree->Fill();
  clear();
  return StatusCode::SUCCESS;
}


StatusCode JetAnalysisAlg :: finalize ()
{

  return StatusCode::SUCCESS;
}

StatusCode JetAnalysisAlg::dumpTruthLLP(){
  const xAOD::TruthParticleContainer *truthParticleCnt = nullptr;
  ATH_CHECK (evtStore()->retrieve (truthParticleCnt, "TruthBSMWithDecayParticles")); // retrieve container
  for (const xAOD::TruthParticle* part : *truthParticleCnt){
    if(part->pdgId()==35){
    mc_alp->push_back(const_cast<xAOD::TruthParticle*>(part));//container 
    mc_alp_energy->push_back(part->e());
    mc_alp_pt->push_back(part->pt());
    mc_alp_m->push_back(part->m());
    mc_alp_eta->push_back(part->eta());
    mc_alp_phi->push_back(part->phi());
    mc_alp_decay_x->push_back(part->decayVtx()->x());
    mc_alp_decay_y->push_back(part->decayVtx()->y());
    mc_alp_decay_z->push_back(part->decayVtx()->z());
    mc_alp_decay_Lxy->push_back(sqrt(pow(part->decayVtx()->x(),2) + pow(part->decayVtx()->y(),2)));
    ATH_MSG_INFO("Lxy of this event " << sqrt(pow(part->decayVtx()->x(),2) + pow(part->decayVtx()->y(),2)));
    }
    if(part->pdgId()==22){
    mc_decay_ph_energy->push_back(part->e());
    mc_decay_ph_pt->push_back(part->pt());
    mc_decay_ph_eta->push_back(part->eta());
    mc_decay_ph_phi->push_back(part->phi());
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode JetAnalysisAlg::dumpTruthZll(){

  const xAOD::TruthParticleContainer *truthParticleCnt = nullptr;
  ATH_CHECK (evtStore()->retrieve (truthParticleCnt, "TruthBoson")); // retrieve container

  for (const xAOD::TruthParticle* part : *truthParticleCnt){
    if(part->pdgId()==23){
    mc_Z_energy->push_back(part->e());
    mc_Z_pt->push_back(part->pt());
    mc_Z_m->push_back(part->m());
    mc_Z_eta->push_back(part->eta());
    mc_Z_phi->push_back(part->phi());
    
    for( size_t i =0 ; i < part->nChildren(); i++ ){
      const xAOD::TruthParticle* lepton = part->child(i);
      if(fabs(lepton->pdgId())== 11){
      mc_Z_el_energy->push_back(lepton->e());
      mc_Z_el_pt->push_back(lepton->pt());
      mc_Z_el_m->push_back(lepton->m());
      mc_Z_el_eta->push_back(lepton->eta());
      mc_Z_el_phi->push_back(lepton->phi());
      }
      if(fabs(lepton->pdgId())== 13){
      mc_Z_mu_energy->push_back(lepton->e());
      mc_Z_mu_pt->push_back(lepton->pt());
      mc_Z_mu_m->push_back(lepton->m());
      mc_Z_mu_eta->push_back(lepton->eta());
      mc_Z_mu_phi->push_back(lepton->phi());
      }
    }    
   }
  }
  return StatusCode::SUCCESS;
}



void JetAnalysisAlg::clear(){
  // ## Jet
  j_jetE->clear();
  j_jetPt->clear();
  j_jetEta->clear();
  j_jetPhi->clear();

  // alp 
  mc_alp_energy->clear();
  mc_alp_pt->clear();
  mc_alp_m->clear();
  mc_alp_eta->clear();
  mc_alp_phi->clear();
  mc_alp_eta->clear();
  mc_alp_decay_x->clear();
  mc_alp_decay_y->clear();
  mc_alp_decay_z->clear();
  mc_alp_decay_Lxy->clear(); 

  // photons 
  mc_decay_ph_energy->clear();
  mc_decay_ph_pt->clear();
  mc_decay_ph_eta->clear();
  mc_decay_ph_phi->clear();

  // ALP jet match
  mc_alp_jet_e->clear();
  mc_alp_jet_pt->clear();
  mc_alp_jet_eta->clear();
  mc_alp_jet_phi->clear();

  /// Z MC truth 
  mc_Z_energy->clear();
  mc_Z_pt->clear();
  mc_Z_m->clear();
  mc_Z_eta->clear();
  mc_Z_phi->clear();
  mc_Z_eta->clear();

  /// Z  electrons and muons
  mc_Z_el_energy->clear();
  mc_Z_el_pt->clear();
  mc_Z_el_m->clear();
  mc_Z_el_eta->clear();
  mc_Z_el_phi->clear();

  mc_Z_mu_energy->clear();
  mc_Z_mu_pt->clear();
  mc_Z_mu_m->clear();
  mc_Z_mu_eta->clear();
  mc_Z_mu_phi->clear();


  // container 
  mc_alp->clear();
  match_j->clear();

}

void JetAnalysisAlg::bookBranches(TTree *tree){
  tree->Branch ("jet_energy",&j_jetE);
  tree->Branch ("jet_pt",&j_jetPt);
  tree->Branch ("jet_eta",&j_jetEta);
  tree->Branch ("jet_phi",&j_jetPhi);

  tree->Branch("alp_energy", &mc_alp_energy);
  tree->Branch("alp_pt", &mc_alp_pt);
  tree->Branch("alp_m", &mc_alp_m);
  tree->Branch("alp_eta", &mc_alp_eta);
  tree->Branch("alp_phi", &mc_alp_phi);
  tree->Branch("alp_eta", &mc_alp_eta);
  tree->Branch("alp_decay_x", &mc_alp_decay_x);
  tree->Branch("alp_decay_y", &mc_alp_decay_y);
  tree->Branch("alp_decay_z", &mc_alp_decay_z); 
  tree->Branch("alp_decay_Lxy", &mc_alp_decay_Lxy);

  // Decay photons 
  tree->Branch("decay_ph_energy", &mc_decay_ph_energy);
  tree->Branch("decay_ph_pt", &mc_decay_ph_pt);
  tree->Branch("decay_ph_eta", &mc_decay_ph_eta);
  tree->Branch("decay_ph_phi", &mc_decay_ph_phi);
  tree->Branch("decay_ph_eta", &mc_decay_ph_eta);

  /// ALP match jets
  tree->Branch("alp_jet_energy", &mc_alp_jet_e);
  tree->Branch("alp_jet_pt", &mc_alp_jet_pt);
  tree->Branch("alp_jet_eta", &mc_alp_jet_eta);
  tree->Branch("alp_jet_phi", &mc_alp_jet_phi);

  /// Z MC truth 
  tree->Branch("Z_energy", &mc_Z_energy);
  tree->Branch("Z_pt", &mc_Z_pt);
  tree->Branch("Z_m", &mc_Z_m);
  tree->Branch("Z_eta", &mc_Z_eta);
  tree->Branch("Z_phi", &mc_Z_phi);
  tree->Branch("Z_eta", &mc_Z_eta);

  /// Z  electrons
  tree->Branch("Z_electrons_energy", &mc_Z_el_energy);
  tree->Branch("Z_electrons_pt", &mc_Z_el_pt);
  tree->Branch("Z_electrons_m", &mc_Z_el_m);
  tree->Branch("Z_electrons_eta", &mc_Z_el_eta);
  tree->Branch("Z_electrons_phi", &mc_Z_el_phi);

  /// Z  muons
  tree->Branch("Z_muons_energy", &mc_Z_mu_energy);
  tree->Branch("Z_muons_pt", &mc_Z_mu_pt);
  tree->Branch("Z_muons_m", &mc_Z_mu_m);
  tree->Branch("Z_muons_eta", &mc_Z_mu_eta);
  tree->Branch("Z_muons_phi", &mc_Z_mu_phi);

}

