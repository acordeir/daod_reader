#ifndef JetAnalysis_Daod
#define JetAnalysis_Daod

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/ReadCondHandleKey.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"

#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODEgamma/PhotonContainer.h" //xAODEgamma = online and xAODegamma = offline
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"

#include <TH1.h>
#include <TTree.h>

#include <bitset>

class JetAnalysisAlg : public AthAlgorithm
{
public:
  // this is a standard algorithm constructor
  JetAnalysisAlg (const std::string& name, ISvcLocator* pSvcLocator);
  
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  virtual StatusCode dumpTruthLLP () ;
  virtual StatusCode  dumpTruthZll();
  void  clear () ;
  void  bookBranches(TTree *tree) ;

  
private:

    Gaudi::Property<std::string> pr_jetName {this, "jetName" , "AntiKt4EMPFlowJets" ,  "Name of the jet container that will have its cells dumped."};


    TTree *m_Tree;

    ServiceHandle<ITHistSvc> m_ntsvc;

    xAOD::TruthParticleContainer* mc_alp = new xAOD::TruthParticleContainer(SG::VIEW_ELEMENTS);

    xAOD::JetContainer* match_j = new xAOD::JetContainer(SG::VIEW_ELEMENTS);

    // Jet
    std::vector < float > *j_jetE = nullptr; // vector to store jets energy 
    std::vector < float > *j_jetPt = nullptr; // transverse momentum of jets 
    std::vector < float > *j_jetEta = nullptr; // vector to store jets eta 
    std::vector < float > *j_jetPhi = nullptr; // vector to store jets phi

    // ## Long-Lived Particle Truth ##
    std::vector < float > *mc_alp_energy   = nullptr;
    std::vector < float > *mc_alp_pt       = nullptr;
    std::vector < float > *mc_alp_m        = nullptr;
    std::vector < float > *mc_alp_eta      = nullptr;
    std::vector < float > *mc_alp_phi      = nullptr;
    std::vector < float > *mc_alp_decay_x      = nullptr;
    std::vector < float > *mc_alp_decay_y      = nullptr;
    std::vector < float > *mc_alp_decay_z      = nullptr;
    std::vector < float > *mc_alp_decay_Lxy      = nullptr;
    
    // #MC Z features
    std::vector < float > *mc_Z_energy   = nullptr;
    std::vector < float > *mc_Z_pt       = nullptr;
    std::vector < float > *mc_Z_m        = nullptr;
    std::vector < float > *mc_Z_eta      = nullptr;
    std::vector < float > *mc_Z_phi      = nullptr;

    // ## Z decay electrons Truth ##
    std::vector < float > *mc_Z_el_energy   = nullptr;
    std::vector < float > *mc_Z_el_pt       = nullptr;
    std::vector < float > *mc_Z_el_m        = nullptr;
    std::vector < float > *mc_Z_el_eta      = nullptr;
    std::vector < float > *mc_Z_el_phi      = nullptr;

    // ## Z decay muons Truth ##
    std::vector < float > *mc_Z_mu_energy   = nullptr;
    std::vector < float > *mc_Z_mu_pt       = nullptr;
    std::vector < float > *mc_Z_mu_m        = nullptr;
    std::vector < float > *mc_Z_mu_eta      = nullptr;
    std::vector < float > *mc_Z_mu_phi      = nullptr;

    // ## Decay Particle Truth ##
    std::vector < float > *mc_decay_ph_energy   = nullptr;
    std::vector < float > *mc_decay_ph_pt       = nullptr;
    std::vector < float > *mc_decay_ph_eta      = nullptr;
    std::vector < float > *mc_decay_ph_phi      = nullptr;

    // match jet
    std::vector < float > *mc_alp_jet_e = nullptr; // vector to store jets energy 
    std::vector < float > *mc_alp_jet_pt = nullptr; // transverse momentum of jets 
    std::vector < float > *mc_alp_jet_eta = nullptr; // vector to store jets eta 
    std::vector < float > *mc_alp_jet_phi = nullptr; // vector to store jets phi

      
};

#endif
