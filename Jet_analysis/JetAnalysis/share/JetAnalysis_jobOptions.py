###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
isMC = True
# doMC = True
dumpCells = False
# doPhoton = False
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import jobproperties as jps

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from JetAnalysis.JetAnalysisConf import JetAnalysisAlg

#### Geometry relation flags
from AthenaCommon.GlobalFlags import jobproperties
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags

#Geometry = "ATLAS-R3S-2021-03-02-00"
globalflags.DetGeo.set_Value_and_Lock('atlas')
#### ! If data is MC, comment the line below! #####
if not(isMC):
    globalflags.DataSource.set_Value_and_Lock('data') #
###################################################
DetFlags.detdescr.all_setOn()
DetFlags.Forward_setOff()
DetFlags.ID_setOff()

#jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have
    # Geometry
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion
#include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py")
include("LArDetDescr/LArDetDescr_joboptions.py")
#####

# Cabling map acess (LAr)
from LArCabling.LArCablingAccess import LArOnOffIdMapping
LArOnOffIdMapping()

job += JetAnalysisAlg( "JetAnalysis" )

#job.JetAnalysis.clusterName                 = "CaloCalTopoClusters"#"LArClusterEM7_11Nocorr"#"LArClusterEM7_11Nocorr"#"egammaClusters"#
job.JetAnalysis.jetName                     = "AntiKt10LCTopoJets"
#job.JetAnalysis.jetName                     = "AntiKt4EMPFlowJets"
#job.JetAnalysis.jetName                     = "HLT_AntiKt4EMTopoJets_nojcalib"

#job.JetAnalysis.jetName                     = "AntiKt4EMTopoJets"
#job.JetAnalysis.doALPMatch                  = True # truth matching jets to ALP or to photons 

#job.JetAnalysis.roiRadius                   = 0.4
#job.JetAnalysis.tileDigName                 = "TileDigitsCnt"
#if isMC:        
#    job.JetAnalysis.larDigName              = "LArDigitContainer_MC"
#else:       
#    job.JetAnalysis.larDigName              = 'FREE' #'LArDigitContainer_EMClust'#'LArDigitContainer_Thinned' #
#job.JetAnalysis.tileRawName                 = "TileRawChannelCnt"
#job.JetAnalysis.larRawName                  = "LArRawChannels"
# job.JetAnalysis.doTile                      = True
#job.JetAnalysis.doClusterDump               = False # Dump only a cluster container. (override the electron cluster)
#job.JetAnalysis.noBadCells                  = True
# job.JetAnalysis.doLAr                       = True
#job.JetAnalysis.printCellsClus              = False
#job.JetAnalysis.printCellsJet               = False
#job.JetAnalysis.testCode                    = False
# Electrons 
#job.JetAnalysis.doTagAndProbe               = False  #select by tag and probe method, electron pairs (start the chain of selection: track + T&P)
#job.JetAnalysis.doElecSelectByTrackOnly     = False  #select only single electrons which pass track criteria (only track)
#job.JetAnalysis.getAssociatedTopoCluster    = True #Get the topo cluster associated to a super cluster, which was linked to an Electron
##
#job.JetAnalysis.OutputLevel                 = INFO #DEBUG # 
#job.JetAnalysis.isMC                        = isMC  # set to True in case of MC sample.



from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool


#inputDataPath = '/eos/user/a/acordeir/A3LP/DAOD/user.lcorpe.601806.PhPy8EG_AZNLO_ggH125_mA0p4_medium.LLP1_AL3Pyy.3_EXT0/'
#inputFile  = [inputDataPath+'user.lcorpe.37235686.EXT0._000014.DAOD_LLP1.pool.root']

inputDataPath = '/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/ANA-EXOT-2023-04_A3LP/DAOD_LLP1/Prod3_Feb2024/user.lcorpe.601805.PhPy8EG_AZNLO_ggH125_mA0p4_prompt.merge.AOD.e8526_e8528_s4111_s4114_r14622_r14663.LLP1_AL3Pyy.3_EXT0/'
inputFile  = [inputDataPath+'user.lcorpe.38198178.EXT0._000011.DAOD_LLP1.pool.root']

ServiceMgr.EventSelector.InputCollections = inputFile
ServiceMgr += CfgMgr.THistSvc()

# Create output file
hsvc = ServiceMgr.THistSvc

hsvc.Output += [ "rec DATAFILE='Topo-electorns-prompt.root' OPT='RECREATE'" ]
theApp.EvtMax = -1

MessageSvc.defaultLimit = 9999999  # all messages

# athena.py JetAnalysis_jobOptions.py  > reader_test_DATA.log 2>&1; code reader_test_DATA.log
